FROM python:3.6-alpine

RUN sed -i -e 's/v3\.4/v3\.6/g' /etc/apk/repositories \
    && apk add --update pgpool \
    && pip install requests

# pgpool setup
RUN mkdir -p /var/run/pgpool /var/log/pgpool \
    && echo "host all all all trust" > /etc/pool_hba.conf

ENV LISTEN_ADDRESSES=* \
    PORT=5432

COPY monitor.py /usr/local/bin/

CMD ["monitor.py"]