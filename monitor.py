#!/usr/bin/env python
import requests
import os
import time
import shutil
import subprocess
import sys
import socket
from urllib.parse import urlparse

BASE_CONFIG_FILE = '/etc/pgpool.conf.sample-stream'
CONFIG_FILE = '/etc/pgpool.conf'
PID_PATH = '/var/run/pgpool/pgpool.pid'
LOG_LEVEL = os.getenv('LOG_LEVEL', 'info')
LOG_STATEMENT = os.getenv('LOG_STATEMENT', 'off')
LOG_PER_NODE_STATEMENT = os.getenv('LOG_PER_NODE_STATEMENT', 'off')
LISTEN_ADDRESSES = os.getenv('LISTEN_ADDRESSES', '*')
PORT = os.getenv('PORT', '9876')
REPLICATION_USER = os.getenv('REPLICATION_USER', 'replicator')
REPLICATION_PASS = os.getenv('REPLICATION_PASS', 'rep-pass')
ETCD_ENDPOINT = os.getenv('ETCD_ENDPOINT', 'http://localhost:2379')

def validate_process(process):
    if process is not None and process.poll() is not None:
        # the pg_pool process quit
        print('pgpool quit:', process.poll(), flush=True)
        # make sure to terminate the process
        process.terminate()
        process.wait()
        return True
    return False

if __name__ == '__main__':
    prev_keys = set()
    pgpool_process = None
    while True:
        # get the members
        try:
            res = requests.get(f'{ETCD_ENDPOINT}/v2/keys/service/governor/members?recursive=true')
        except requests.exceptions.ConnectionError:
            res = None
            print(f'Could not connect to etcd at {ETCD_ENDPOINT}. Will try again in 15 seconds.', flush=True)

        if res is not None and res.status_code == 200:
            json = res.json()

            # make sure there is a JSON response
            if json is not None and 'node' in json and 'nodes' in json['node']:
                nodes = json['node']['nodes']
                new_keys = set(n['key'] for n in nodes)

                # check if there is a difference
                if new_keys != prev_keys:
                    print(f'Adding members to the config: {new_keys}', flush=True)

                    # copy the base config to the config location
                    shutil.copy(BASE_CONFIG_FILE, CONFIG_FILE)

                    # open the config file
                    with open(CONFIG_FILE, 'a') as f:
                        f.write(f'log_min_messages = {LOG_LEVEL}\n')

                        # iterate through the nodes and add them to the config
                        for i, node in enumerate(nodes):
                            host = node['key'].split('/')[-1]
                            address = node['value']
                            url = urlparse(address)

                            while True:
                                try:
                                    res = requests.get(f'{ETCD_ENDPOINT}/v2/keys/service/governor/ready/{host}')
                                    if res and res.status_code == 404:
                                        print(f'Host is not ready: {url.hostname}:{url.port}. Will try again in 5 seconds.', flush=True)
                                        continue
                                    break
                                except requests.exceptions.ConnectionError:
                                    print(f'Could not connect to etcd at {ETCD_ENDPOINT}. Will try again in 5 seconds.', flush=True)
                                time.sleep(5)

                            f.write(f'backend_hostname{i} = \'{url.hostname}\'\n'
                                    f'backend_port{i} = {url.port}\n'
                                    f'backend_weight{i} = 1\n'
                                    f'backend_data_directory{i} = \'/var/lib/postgresql/data/{host}\'\n'
                                    f'backend_flag{i} = \'DISALLOW_TO_FAILOVER\'\n\n')
                            # this is set to disallow because governor is handling failover

                    # replace the listen_addresses and port
                    subprocess.run(['sed', '-i',
                        f'-e s/listen_addresses = \'localhost\'/listen_addresses = \'{LISTEN_ADDRESSES}\'/',
                        f'-e s/load_balance_mode = off/load_balance_mode = on/',
                        f'-e s/master_slave_mode = off/master_slave_mode = on/',
                        f'-e s/master_slave_sub_mode = \'slony\'/master_slave_sub_mode = \'stream\'/',
                        f'-e s/health_check_period = 0/health_check_period = 15/',
                        f'-e s/health_check_user = \'nobody\'/health_check_user = \'postgres\'/',
                        f'-e s/health_check_database = \'\'/health_check_database = \'postgres\'/',
                        # f'-e s/health_check_password = \'\'/health_check_password = \'{REPLICATION_PASS}\'/',
                        f'-e s/health_check_max_retries = 0/health_check_max_retries = 10000/',
                        f'-e s/sr_check_period = 0/sr_check_period = 15/',
                        f'-e s/sr_check_user = \'nobody\'/sr_check_user = \'postgres\'/',
                        # f'-e s/sr_check_password = \'\'/sr_check_password = \'{REPLICATION_PASS}\'/',
                        f'-e s/log_statement = off/log_statement = {LOG_STATEMENT}/',
                        f'-e s/log_per_node_statement = off/log_per_node_statement = {LOG_PER_NODE_STATEMENT}/',
                        f'{CONFIG_FILE}'], check=True)

                    # subprocess.run(['sed', '-i',
                    #     f's/port = 9999/port = {PORT}/',
                    #     f'{CONFIG_FILE}'], check=True)

                    # restart pgpool
                    print('Starting pgpool with new configuration...', flush=True)
                    try:
                        command = ['pgpool', '-D', '-n']
                        if validate_process(pgpool_process) and os.path.isfile(PID_PATH):
                            # process was terminated. rm the pid file
                            os.remove(PID_PATH)

                        # is pgpool already running?
                        if os.path.isfile(PID_PATH) and pgpool_process is not None:
                            subprocess.run(['pgpool', 'reload'], check=True)
                            print('Reloaded pgpool', flush=True)
                        else:
                            pgpool_process = subprocess.Popen(['pgpool', '-n'], stdout=sys.stdout, stderr=sys.stderr)
                            if pgpool_process.poll() is None:
                                print('Started pgpool', flush=True)
                        
                        if pgpool_process.poll() is None:
                            prev_keys = new_keys
                        else:
                            print('Could not start pgpool. Will retry in 15 seconds', flush=True)
                    except subprocess.CalledProcessError as err:
                        print(err, flush=True)
                        print('Could not start pgpool. Will retry in 15 seconds', flush=True)
        elif res is not None and res.status_code != 200:
            print(f'Could not get members: {res.json()}', flush=True)

        # sleep so we're not hammering the etcd endpoint
        time.sleep(15)
